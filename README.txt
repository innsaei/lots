██╗      ██████╗ ████████╗███████╗
██║     ██╔═══██╗╚══██╔══╝██╔════╝
██║     ██║   ██║   ██║   ███████╗
██║     ██║   ██║   ██║   ╚════██║
███████╗╚██████╔╝   ██║   ███████║
╚══════╝ ╚═════╝    ╚═╝   ╚══════╝
                                  
Lord of the Stones :D

This is a plain javascript GO App which allows to play the game of Go https://en.wikipedia.org/wiki/Go_(game)

Functionalities
  - Responsive or fixed width.
  - Customizable board size (even non traditional sizes).
  - Customizable assets.
  - Captures stones.
  - Prevents illegal moves (preexisting board states, suicide).

TODO:
  - End game after two passes.
  - FLood fill + Blob alg. implementation to count points.
  - Import/ Export SGF.
  - History tree.
  - Go to point in history.
  - Generate iframe (will need some php).

Rules:
  1) The board is empty at the onset of the game (unless players agree to place a handicap).
  2) Black makes the first move, after which White and Black alternate.
  3) A move consists of placing one stone of one's own color on an empty intersection on the board.
  4) A player may pass their turn at any time.
  5) A stone or solidly connected group of stones of one color is captured and removed from the board when all the intersections directly adjacent to it are occupied by the enemy. (Capture of the enemy takes precedence over self-capture.)
  6) No stone may be played so as to recreate a former board position.
  7) Two consecutive passes end the game.
  8) A player's territory consists of all the points the player has either occupied or surrounded.
  9) The player with more territory wins.

  More info: https://en.wikipedia.org/wiki/Rules_of_Go
